# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  word_hash = {}
  str.split.each do |word|
    word_hash[word] = word.length
  end
  word_hash
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by {|k, v| v}[-1][0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |k, v|
    older[k] = v
  end
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  letter_hash = Hash.new(0)
  word.each_char do |letter|
    letter_hash[letter] += 1
  end
  letter_hash
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  counter = Hash.new(0)
  arr.each {|el| counter[el] += 1}
  counter.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  even_or_odd = {even: 0, odd: 0}
  numbers.each do |num|
    if num.even?
      even_or_odd[:even] += 1
    else
      even_or_odd[:odd] += 1
    end
  end
  even_or_odd
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowels = ['a', 'e', 'i', 'o', 'u']

  vowel_counter = Hash.new(0)
  string.each_char do |letter|
    if vowels.include?(letter)
      vowel_counter[letter] += 1
    end
  end
  max = vowel_counter.values.max
  vowel_counter.select {|k, v| v == max}.keys.min
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  names = students.select {|k, v| v > 6}.keys
  name_combinations = []
  (0..names.length - 2).each do |idx1|
    (idx1 + 1..names.length - 1).each do |idx2|
      name_combinations << [names[idx1], names[idx2]]
    end
  end
  name_combinations
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  species = Hash.new(0)
  specimens.each do |s|
    species[s] += 1
  end

  num_species = species.keys.count
  smallest_pop = species.sort_by {|k, v| v}[0][1]
  largest_pop = species.sort_by {|k, v| v}[-1][1]

  return num_species ** 2 * smallest_pop / largest_pop
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_char = character_count(normal_sign)
  vandalized_char = character_count(vandalized_sign)

  vandalized_char.each do |ch, v|
    return false if v > normal_char[ch]
  end
  true
end

def character_count(str)
  char_counter = Hash.new(0)
  str.downcase.delete('.,;\'?! ').each_char do |ch|
    char_counter[ch] += 1
  end
  char_counter
end
